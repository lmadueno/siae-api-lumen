<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {    
    $router->post('/ciudadano/create', 'CiudadanoController@create');
	$router->post('/ciudadano/login', 'CiudadanoController@login');
	$router->post('/ciudadano/update', 'CiudadanoController@update');
	$router->post('/ciudadano/update_pass', 'CiudadanoController@update_pass');
	$router->post('/ciudadano/loginfb', 'CiudadanoController@loginfb');
	$router->post('/ciudadano/createfb', 'CiudadanoController@createfb');
	$router->post('/ciudadano/reporte', 'CiudadanoController@reporte');
	$router->post('/ciudadano/notifica', 'CiudadanoController@notifica');
	$router->post('/ciudadano/findById', 'CiudadanoController@findById');



	/*$router->post('/ciudadanoinsertincidencia', 'CiudadanoController@envioincidencia');
	$router->post('/ciudadanoupdateRegister', 'CiudadanoController@updateuserregister');
	$router->post('/ciudadanoupdateRegisterApp', 'CiudadanoController@updateUserRegisterApp');*/
});
