<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CiudadanoController extends Controller
{
    function create(Request $request)
    {
        if($request->isJson()){
			$data = app('db')->select("select * from sae.fi_persona('" . $request->nro_doc_ide. "','" . $request->telefono. "', '" . $request->correo. "','" . md5($request->pass). "')");
			return response()->json(json_decode($data{0}->fi_persona), 200);
        }
        return response()->json(['error' => 'Unauthorized'], 400);
		
    }
	function createfb(Request $request)
    {
        if($request->isJson()){
			$data = app('db')->select("select * from sae.fi_personafb('" . $request->nro_doc_ide. "','" . $request->telefono. "', '" . $request->correo. "','" . $request->nombres. "','" . $request->ape_pat. "','" . $request->ape_mat. "','" . $request->idorigen. "','" . $request->origen. "','" . md5($request->pass). "')");
			return response()->json(json_decode($data{0}->fi_personafb), 200);
        }
        return response()->json(['error' => 'Unauthorized'], 400);
		
    }
	function login(Request $request)
    {
		if($request->isJson()){			
			$data = app('db')->select("select * from sae.fs_login('" . $request->telefono. "', '" . md5($request->nro_doc_ide). "')");
			return response()->json(json_decode($data{0}->fs_login), 200);
		}
		
		return response()->json(['error' => 'Unauthorized'], 400);
    }
	function loginfb(Request $request)
    {
		if($request->isJson()){			
			$data = app('db')->select("select * from sae.fs_loginfb('" . $request->nombres. "','" . $request->email. "', '" . $request->origen. "', '" . $request->idorigen. "')");
			return response()->json(json_decode($data{0}->fs_loginfb), 200);
		}
		
		return response()->json(['error' => 'Unauthorized'], 400);
    }
	function reporte(Request $request)
    {
        if($request->isJson()){         
            $data = app('db')->select("select * from sae.fs_reporte_incidencia( '" .$request->nro_doc_ide. "' )");
            return response()->json(json_decode($data{0}->fs_reporte_incidencia), 200);
        }
        
        return response()->json(['error' => 'Unauthorized'], 400);
    }
	
	function update(Request $request)
    {		
        if($request->isJson()){
			$data = app('db')->select("select * from sae.fu_persona('" . $request->nro_doc_ide. "','" . $request->nombre. "', '" . $request->pre_nombre. "','" . $request->ape_pat. "', '" . $request->ape_mat. "')");
			return response()->json(json_decode($data{0}->fu_persona), 200);
        }
        return response()->json(['error' => 'Unauthorized'], 400);
    }
	function update_pass(Request $request)
    {		
        if($request->isJson()){
			if($request->factor=='1'){
			$data = app('db')->select("select * from sae.fu_persona_vali_pass('" . $request->correo. "','" . $request->telefono. "', '" . $request->nro_doc_ide. "','" . $request->factor. "')");
			return response()->json(json_decode($data{0}->fu_persona_vali_pass), 200);
			}else{
				$data = app('db')->select("select * from sae.fu_persona_vali_pass('" . $request->correo. "','" . $request->telefono. "', '" . md5($request->nro_doc_ide). "','" . $request->factor. "')");
			return response()->json(json_decode($data{0}->fu_persona_vali_pass), 200);
			}
        }
        return response()->json(['error' => 'Unauthorized'], 400);
    }

	function notifica(Request $request)
    {
        if($request->isJson()){         
            $data = app('db')->select("select * from sae.fs_notifica_lista( '" .$request->nro_doc_ide. "' )");
            return response()->json(json_decode($data{0}->fs_notifica_lista), 200);
        }
        
        return response()->json(['error' => 'Unauthorized'], 400);
    }
    
	function findById(Request $request){
		$where = [];
		$sql   = []; 		
		$method = $request->method;
		
		return CiudadanoController::$method($where, $sql, $request);
	}
	
	function comp_linea_x_dia($where, $sql, $request){
		//$pg  = new PgSql();
           
		$subquery = "select
         x.valor,
         COUNT(case when estado_atencion = 'P' then 1 end) as pendientes,
         COUNT(case when estado_atencion = 'T' then 1 end) as en_proceso,
         COUNT(case when estado_atencion = 'C' then 1 end) as concluido
        from(
         select to_char(now(), 'dd/mm/yyyy') as valor union all
         select to_char(now() - CAST('1 days' AS INTERVAL), 'dd/mm/yyyy')
        ) x
        inner join sae.ssc_incidente inc on to_char(inc.fecha_registro, 'dd/mm/yyyy') = x.valor
		where " . (count($where) > 0 ? join(" and ", $where) : '1 = 1')  . "
        group by
         x.valor
		order by
	     x.valor";
		 
		/*if(isset($request->table)){
			return $pg->getRows($subquery);
		}*/
        $data = app('db')->select("select
        row_to_json(x) 
       FROM(
        select
         (select row_to_json(t) from(select 'column' as type) t) as chart,
         (select row_to_json(t) from(select 'Comparativo por estado de incidencia en los 2 últimos días' as text) t) as title,
         (select row_to_json(t) from(select false as enabled) t) as credits,
         (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS xAxis,
         (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as yAxis,
         (select row_to_json(t) from(select true as enabled) t) as exporting,
         (  
          select
           array_to_json(array_agg(x)) as series
          from(      
           select 'Pendientes' as name, JSON_AGG(x.pendientes) as data union all
           select 'En proceso' as name, JSON_AGG(x.en_proceso) as data union all
           select 'Concluidos' as name, JSON_AGG(x.concluido) as data
          ) x
         )
        from(
         " . $subquery . "
        ) x
       ) x");
		
        return response()->json(json_decode($data{0}->row_to_json), 200);
	}
	function comp_linea_x_sem($where, $sql, $params){
		//$pg    = new PgSql();
		
		$subquery = "select
         x.valor,
         COUNT(case when estado_atencion = 'P' then 1 end) as pendientes,
         COUNT(case when estado_atencion = 'T' then 1 end) as en_proceso,
         COUNT(case when estado_atencion = 'C' then 1 end) as concluido
        from(
         select extract(week from now())::text || extract(year from now())::text as valor union all
         select extract(week from now() - CAST('1 week' AS INTERVAL)) || extract(year from now() - CAST('1 week' AS INTERVAL))::text
        ) x
        inner join sae.ssc_incidente inc on extract(week from inc.fecha_registro)::text || extract(year from inc.fecha_registro)::text = x.valor
        group by
         x.valor
        order by
         x.valor";
		 
		// if(isset($params->table)){
		// 	return $pg->getRows($subquery);
		// }
		
		$data = app('db')->select("select
         row_to_json(x) 
        FROM(
         select
          (select row_to_json(t) from(select 'column' as type) t) as chart,
          (select row_to_json(t) from(select 'Comparativo por estado de incidencia en los 2 últimos días' as text) t) as title,
          (select row_to_json(t) from(select false as enabled) t) as credits,
          (select Row_to_json(t) FROM(select json_agg('S' || substr(x.valor,1,2)) AS categories) t) AS xAxis,
          (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as yAxis,
          (select row_to_json(t) from(select true as enabled) t) as exporting,
          (  
           select
            array_to_json(array_agg(x)) as series
           from(      
            select 'Pendientes' as name, JSON_AGG(x.pendientes) as data union all
			select 'En proceso' as name, JSON_AGG(x.en_proceso) as data union all
			select 'Concluidos' as name, JSON_AGG(x.concluido) as data
           ) x
          )
         from(
          " . $subquery . "
         ) x
        ) x");
        return response()->json(json_decode($data{0}->row_to_json), 200);

		//return $arr->row_to_json;
	}
	
	function comp_linea_x_mes($where, $sql, $params){
		//$pg    = new PgSql();
		
		$subquery = "select
         x.valor,
         COUNT(case when estado_atencion = 'P' then 1 end) as pendientes,
         COUNT(case when estado_atencion = 'T' then 1 end) as en_proceso,
         COUNT(case when estado_atencion = 'C' then 1 end) as concluido
        from(
         select to_char(now(), 'mm/yyyy') as valor union all
	     select to_char(now() - CAST('1 month' AS INTERVAL), 'mm/yyyy')
        ) x
        inner join sae.ssc_incidente inc on to_char(inc.fecha_registro, 'mm/yyyy') = x.valor
		where " . (count($where) > 0 ? join(" and ", $where) : '1 = 1')  . "
        group by
         x.valor
		order by
	     x.valor";
		 
		// if(isset($params->table)){
		// 	return $pg->getRows($subquery);
		// }
		
		$data = app('db')->select("select
         row_to_json(x) 
        FROM(
         select
          (select row_to_json(t) from(select 'column' as type) t) as chart,
          (select row_to_json(t) from(select 'Comparativo por estado de incidencia en los 2 últimos meses' as text) t) as title,
          (select row_to_json(t) from(select false as enabled) t) as credits,
          (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS \"xaxis\",
          (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as \"yaxis\",
          (select row_to_json(t) from(select true as enabled) t) as exporting,
          (  
           select
            array_to_json(array_agg(x)) as series
           from(      
            select 'Pendientes' as name, JSON_AGG(x.pendientes) as data union all
			select 'En proceso' as name, JSON_AGG(x.en_proceso) as data union all
			select 'Concluidos' as name, JSON_AGG(x.concluido) as data
           ) x
          )
         from(
          " . $subquery . "
         ) x
        ) x");
		return response()->json(json_decode($data{0}->row_to_json), 200);

//		return $arr->row_to_json;
	}
    function comp_cat_x_grupo_dia($where, $sql, $params){
		//$pg    = new PgSql();
		
		$subquery = "select 
		 mt.valor,
		 COUNT(case when extract(isodow from inc.fecha_registro)::integer in (1,2,3,4,5) then inc.incidente_id end) as l_v,
		 COUNT(case when extract(isodow from inc.fecha_registro)::integer in (6) then inc.incidente_id end) as sbd,
		 COUNT(case when extract(isodow from inc.fecha_registro)::integer in (7) then inc.incidente_id end) as dmg
		from sae.ssc_multitabla mt
		inner join sae.ssc_incidente inc on inc.clasificacion_incidente_id = mt.multitabla_id
		where " . (count($where) > 0 ? join(" and ", $where) : '1 = 1')  . "
		group by
		 mt.valor";
		 
		// if(isset($params->table)){
		// 	return $pg->getRows($subquery);
		// }
		
		$data = app('db')->select("select
         row_to_json(x) 
        FROM(
         select
          (select row_to_json(t) from(select 'column' as type) t) as chart,
          (select row_to_json(t) from(select 'Comparativo de categorías por tipo de día' as text) t) as title,
          (select row_to_json(t) from(select false as enabled) t) as credits,
          (select Row_to_json(t) FROM(select json_agg(x.valor) AS categories) t) AS xaxis,
          (select row_to_json(t) from (select row_to_json(t) as title from(select 'Cantidad de incidentes' as text) t) t) as yaxis,
          (select row_to_json(t) from(select true as enabled) t) as exporting,
          (  
           select
            array_to_json(array_agg(x)) as series
           from(      
            select 'Día hábil' as name, JSON_AGG(x.l_v) as data union all
			select 'Sábado' as name, JSON_AGG(x.sbd) as data union all
			select 'Domingo' as name, JSON_AGG(x.dmg) as data
           ) x
          )
         from(
          " . $subquery . "
         ) x
        ) x");
        return response()->json(json_decode($data{0}->row_to_json), 200);

		//return $arr->row_to_json;
	}
}
