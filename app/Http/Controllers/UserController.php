<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    function index(Request $request)
    {
        if($request->isJson()){
            $data = app('db')->select("select * from ssc.fs_test()");
            return response()->json($data, 200);
        }
        
        return response()->json(['error' => 'Unauthorized'], 400);
    }
}
